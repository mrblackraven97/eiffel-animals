note
	description: "animals application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a: ANIMAL
			c: COW
			c2: COW
			g: GRASS
			f: FOOD
		do
			create c
			create c2
			create g
			a := c
			f := g

			g.grow (20)

			-- staticamente questa chiamata va benissimo perch�
			-- a � un Animal e il suo metodo eat(Food f) � compatibile con l'argomento passato (Food)
			-- no errori di compilazione !

			-- dinamicamente va anche bene, perch�
			-- a � in realt� una Cow e il suo metodo eat(Grass g) � compatibile con l'argomento passato (Grass)
			-- no catcalls!
			print (a.out + " is going to eat: " + f.out + "%N")
			a.eat (f)

			-- staticamente questa chiamata va benissimo perch�
			-- a � un Animal e il suo metodo eat(Food f) � compatibile con l'argomento passato (Grass, subclass of Food)
			-- questo vale solamente perch� eiffel � covariante, quindi no errori di compilazione !

			-- dinamicamente va anche bene, perch�
			-- a � in realt� una Cow e il suo metodo eat(Grass g) � compatibile con l'argomento passato (Grass)
			-- no catcalls!
			print (a.out + " is going to eat: " + f.out + "%N")
			a.eat (g)



			-- staticamente questa chiamata FALLISCE perch�
			-- c � una Cow e il suo metodo eat(Grass g) non � compatibile con l'argomento passato (Food)
			-- questo perch� Food potrebbe essere in realt� qualsiasi cosa, non � detto che sia Grass
			-- e ci� viola il contratto del metodo : ho quindi un errore di compilazione
			--c.eat (f)



			-- staticamente questa chiamata va benissimo perch�
			-- a � un Animal e il suo metodo eat(Food f) � compatibile con l'argomento passato (Food)
			-- no errori di compilazione !

			-- dinamicamente invece fallisce perch�
			-- f in realt� � una Cow e a � una Cow quindi avrei una Cow che chiama eat(Cow) che non � fattibile
			-- dato che Cow mangia solo grass eat(Grass)
			-- in questo caso ricever� una CATCALL
			--f := c
			--a.eat (f)


			-- anche in questo caso staticamente va benissimo
			-- dinamicamente invece fallisce perch� ho una Cow che mangia s� stessa (una Cow)
			-- inoltre avrei anche non rispettato il contratto no_autofagy
			--f := a
			--a.eat (f)

			-- anche qui catcall ma staticamente va bene: il contratto no_autofagy per� � rispettato
			--f := c2
			--a.eat (f)




		end

end
