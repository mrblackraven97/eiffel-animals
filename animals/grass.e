note
	description: "Summary description for {GRASS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GRASS

inherit

	FOOD
		redefine
			out
		end

feature

	out: STRING
		do
			Result := "a bunch of grass (" + weight.out + "kg)"
		end

	consume (q: INTEGER)
		require
			enough: weight >= q

		do
			weight := weight - q

		ensure
			reduced_weight: weight = old weight - q

		end

	grow (q: INTEGER)
		require
			max_growth: weight < 1000
		do
			weight := weight + q
		ensure
			added_weight: weight = old weight + q

		end

	weight: INTEGER

	-- invariants are always put together with feature preconditions
	invariant
		positive_weight: weight >= 0

end
